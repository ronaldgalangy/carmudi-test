# Carmudi Mobile Developer Test

### Goals
- [x] Maintain View state across orientation changes.
- [x] Display list of vehicle image, name, price and brand.
- [x] Display details of Vehicle.
- [x] Pull-To-Refresh functionality.
- [x] Infinite scrolling.
- [x] Sorting functionality.
- [x] Implement MVP Pattern.
- [ ] Offline functionality.
- [ ] Unit Test.

### IDE
* Android Studio 2.3.3

### Build System
* Gradle
* Java 8 (1.8.0_144)
