package rygalang.com.carmuditest.util;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Computer3 on 10/9/2017.
 */
@Singleton
public class PrefsHelper {
    private SharedPreferences prefs;

    @Inject
    public PrefsHelper(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public String getString(String key) {
        return prefs.getString(key, "");
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public float getFloat(String key) {
        return prefs.getFloat(key, 0f);
    }

    public long getLong(String key) {
        return prefs.getLong(key, 0L);
    }

    public boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public void setBoolean(String key,
                           boolean value) {
        prefs.edit().putBoolean(key, value).commit();
    }

    public void setString(String key, String value) {
        prefs.edit().putString(key, value).commit();
    }

    public void setInt(String key, int value) {
        prefs.edit().putInt(key, value).commit();
    }

    public void setFloat(String key, Float value) {
        prefs.edit().putFloat(key, value).commit();
    }

    public void setLong(String key, Long value) {
        prefs.edit().putLong(key, value).commit();
    }
}
