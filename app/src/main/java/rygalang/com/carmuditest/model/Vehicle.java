package rygalang.com.carmuditest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class Vehicle implements Parcelable {
    private long id;
    private VehicleData data;
    private List<VehicleImage> images;

    public Vehicle() {
    }

    protected Vehicle(Parcel in) {
        id = in.readLong();
        data = in.readParcelable(VehicleData.class.getClassLoader());
        images = in.readArrayList(VehicleImage.class.getClassLoader());
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public VehicleData getData() {
        return data;
    }

    public void setData(VehicleData data) {
        this.data = data;
    }

    public List<VehicleImage> getImages() {
        return images;
    }

    public void setImages(List<VehicleImage> images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeParcelable(data, flags);
        parcel.writeList(images);
    }
}
