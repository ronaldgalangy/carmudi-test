package rygalang.com.carmuditest.model;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class VehicleResponse {
    private boolean success;
    private MetaData metadata;

    public VehicleResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public MetaData getMetaData() {
        return metadata;
    }

    public void setMetaData(MetaData metadata) {
        this.metadata = metadata;
    }
}
