package rygalang.com.carmuditest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class VehicleData implements Parcelable {
    private String name;
    private String brand;
    private String price;
    private JsonObject attributes;

    public VehicleData() {
    }

    protected VehicleData(Parcel in) {
        name = in.readString();
        brand = in.readString();
        price = in.readString();
        attributes = new Gson().fromJson(in.readString(), JsonObject.class);
    }

    public static final Creator<VehicleData> CREATOR = new Creator<VehicleData>() {
        @Override
        public VehicleData createFromParcel(Parcel in) {
            return new VehicleData(in);
        }

        @Override
        public VehicleData[] newArray(int size) {
            return new VehicleData[size];
        }
    };

    public JsonObject getAttributes() {
        return attributes;
    }

    public void setAttributes(JsonObject attributes) {
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeString(brand);
        parcel.writeString(price);
        parcel.writeString(attributes.toString());
    }
}
