package rygalang.com.carmuditest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class OptionalItem implements Parcelable {
    private String name;
    private String[] options;

    public OptionalItem() {
    }

    public OptionalItem(String name, String[] options) {
        this.name = name;
        this.options = options;
    }

    protected OptionalItem(Parcel in) {
        name = in.readString();
        options = in.createStringArray();
    }

    public static final Creator<OptionalItem> CREATOR = new Creator<OptionalItem>() {
        @Override
        public OptionalItem createFromParcel(Parcel in) {
            return new OptionalItem(in);
        }

        @Override
        public OptionalItem[] newArray(int size) {
            return new OptionalItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeArray(this.options);
    }
}
