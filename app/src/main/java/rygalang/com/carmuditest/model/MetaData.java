package rygalang.com.carmuditest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class MetaData {
    @SerializedName("product_count")
    private long productCount;
    private ArrayList<Vehicle> results;

    public MetaData() {
    }

    public long getProductCount() {
        return productCount;
    }

    public void setProductCount(long productCount) {
        this.productCount = productCount;
    }

    public ArrayList<Vehicle> getResults() {
        return results;
    }

    public void setResults(ArrayList<Vehicle> results) {
        this.results = results;
    }
}
