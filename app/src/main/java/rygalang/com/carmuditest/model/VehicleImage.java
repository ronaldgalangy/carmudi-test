package rygalang.com.carmuditest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class VehicleImage implements Parcelable {
    private String url;

    public VehicleImage() {
    }

    protected VehicleImage(Parcel in) {
        url = in.readString();
    }

    public static final Creator<VehicleImage> CREATOR = new Creator<VehicleImage>() {
        @Override
        public VehicleImage createFromParcel(Parcel in) {
            return new VehicleImage(in);
        }

        @Override
        public VehicleImage[] newArray(int size) {
            return new VehicleImage[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
    }
}
