package rygalang.com.carmuditest.base;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Computer3 on 4/12/2017.
 */

public abstract class BaseFragment<T extends BaseActivity> extends Fragment {

    private T activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (T) context;
    }



    public void showFragment(BaseFragment fragment, String tag) {
        final FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.findFragmentById(fragment.getId()) != null) {
            fragmentManager.popBackStackImmediate(tag, 0);
        } else {
            /*fragmentManager.beginTransaction()
                    .replace(R.id., fragment, tag).addToBackStack(tag).
                    commit();*/
        }
    }

    public T getParentActivity() {
        return activity;
    }
}
