package rygalang.com.carmuditest.base;

/**
 * Created by Computer3 on 6/8/2017.
 */

public interface BaseView {
    void showLoading();

    void dismissLoading();
}
