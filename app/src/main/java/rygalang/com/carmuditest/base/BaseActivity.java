package rygalang.com.carmuditest.base;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import rygalang.com.carmuditest.App;
import rygalang.com.carmuditest.BuildConfig;

/**
 * Created by Computer3 on 4/12/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    private static ProgressDialog progressDialog;


    public boolean isServiceRunning(Class clz) {
        final ActivityManager activityManager = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (runningServiceInfo.service.getClassName()
                    .equals(clz.getSimpleName())) {
                return true;
            }
        }
        return false;
    }

    public void showConfirmationDialog(final String message, final String positiveButtonText,
                                       final String negativeButtonText,
                                       final OnConfirmationListener listener) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (listener != null) {
                    listener.onPositiveButtonClick();
                }
            }
        });

        if (negativeButtonText != null) {
            alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onNegativeButtonClick();
                    }
                }
            });
        }

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void log(String txt) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, txt);
        }
    }

    public boolean hasActiveInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public interface OnSecurityKeyDialogListener {
        void onConfirmed(String securityKey);
    }

    public interface OnConfirmationListener {
        void onPositiveButtonClick();

        void onNegativeButtonClick();
    }

    public interface OnDialogListener {
        void onSubmitNote(String emailAddress);
    }

    public App getApp() {
        return (App) getApplication();
    }
}
