package rygalang.com.carmuditest;

import android.app.Application;

import rygalang.com.carmuditest.component.DaggerNetComponent;
import rygalang.com.carmuditest.component.NetComponent;
import rygalang.com.carmuditest.modules.AppModule;
import rygalang.com.carmuditest.modules.NetModule;

/**
 * Created by Computer3 on 10/6/2017.
 */

public class App extends Application {
    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.HOST_URL))
                .build();
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
}
