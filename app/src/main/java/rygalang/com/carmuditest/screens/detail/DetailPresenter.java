package rygalang.com.carmuditest.screens.detail;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import rygalang.com.carmuditest.base.BasePresenter;
import rygalang.com.carmuditest.model.OptionalItem;
import rygalang.com.carmuditest.model.Vehicle;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class DetailPresenter extends BasePresenter implements DetailContract.DetailAction {
    private DetailContract.DetailView detailView;
    private Vehicle vehicle;
    private CompositeDisposable compositeDisposable;

    @Inject
    public DetailPresenter(DetailContract.DetailView detailView, Vehicle vehicle,
                           CompositeDisposable compositeDisposable) {
        this.detailView = detailView;
        this.vehicle = vehicle;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void getVehicleAttributes() {
        final JsonArray details = vehicle.getData().getAttributes().get("all").getAsJsonObject().get("details").getAsJsonArray();
        final JsonArray optionals = vehicle.getData().getAttributes().get("all").getAsJsonObject().get("optional").getAsJsonArray();
        compositeDisposable.add(filterDetails(details)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<JsonArray>() {
                    @Override
                    public void accept(JsonArray jsonElements) throws Exception {
                        detailView.displayAttributes(jsonElements);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                }));
        compositeDisposable.add(transformOptionals(optionals)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<OptionalItem>>() {
                    @Override
                    public void accept(List<OptionalItem> optionals) throws Exception {
                        detailView.displayOptionals(optionals);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                }));
        detailView.displayVehicleImages(vehicle.getImages());
    }

    private Observable<JsonArray> filterDetails(final JsonArray details) {
        return Observable.fromCallable(new Callable<JsonArray>() {
            @Override
            public JsonArray call() throws Exception {
                final JsonArray filteredDetails = new JsonArray();
                for (int i = 0; i < details.size(); i++) {
                    String value = details.get(i).getAsJsonObject().get("value").getAsString();
                    if (!value.isEmpty()) {
                        filteredDetails.add(details.get(i));
                    }
                }
                return filteredDetails;
            }
        });
    }

    private Observable<List<OptionalItem>> transformOptionals(final JsonArray optionals) {
        return Observable.fromCallable(new Callable<List<OptionalItem>>() {
            @Override
            public List<OptionalItem> call() throws Exception {
                final List<OptionalItem> list = new ArrayList<>();
                for (JsonElement optional : optionals) {
                    final OptionalItem item = new OptionalItem();
                    final String[] optionsArray = new Gson().fromJson(optional.getAsJsonObject().get("options"), String[].class);
                    item.setOptions(optionsArray);
                    item.setName(optional.getAsJsonObject().get("label_en").getAsString());
                    list.add(item);
                }
                return list;
            }
        });
    }

    @Override
    protected void destroy() {
        compositeDisposable.clear();
    }
}
