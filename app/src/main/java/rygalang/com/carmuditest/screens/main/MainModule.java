package rygalang.com.carmuditest.screens.main;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import rygalang.com.carmuditest.model.Vehicle;

/**
 * Created by Computer3 on 10/9/2017.
 */
@Module
public class MainModule {
    private MainContract.MainView mainView;

    public MainModule(MainContract.MainView mainView) {
        this.mainView = mainView;
    }

    @Provides
    public MainContract.MainView provideMainView() {
        return mainView;
    }

    @Provides
    public VehicleAdapter provideVehicleAdapter() {
        return new VehicleAdapter(new ArrayList<Vehicle>());
    }

    @Provides
    public CompositeDisposable proviceCompositeDisposable() {
        return new CompositeDisposable();
    }
}
