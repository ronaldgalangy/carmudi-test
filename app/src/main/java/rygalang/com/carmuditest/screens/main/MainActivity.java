package rygalang.com.carmuditest.screens.main;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import javax.inject.Inject;

import rygalang.com.carmuditest.R;
import rygalang.com.carmuditest.base.BaseActivity;
import rygalang.com.carmuditest.databinding.ActivityMainBinding;
import rygalang.com.carmuditest.model.Vehicle;
import rygalang.com.carmuditest.util.EndlessRecyclerViewScrollListener;

public class MainActivity extends BaseActivity implements MainContract.MainView, SwipeRefreshLayout.OnRefreshListener {
    private static final int PER_PAGE = 20;
    private static final String ITEMS = "items";
    private static final String CURRENT_PAGE = "current_page";
    private ActivityMainBinding mainBinding;
    private long totalPages;
    private EndlessRecyclerViewScrollListener scrollListener;
    private String sortKey = "";

    @Inject
    MainPresenter mainPresenter;
    @Inject
    VehicleAdapter vehicleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .netComponent(getApp().getNetComponent())
                .build().inject(this);

        scrollListener = new EndlessRecyclerViewScrollListener(
                (LinearLayoutManager) mainBinding.dataList.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (page != totalPages) {
                    mainBinding.progressBar.setVisibility(View.VISIBLE);
                    mainPresenter.getVehicles(page + 1, PER_PAGE, sortKey);
                } else {
                    showToast("No more items.");
                    mainBinding.progressBar.setVisibility(View.GONE);
                }
            }
        };

        mainBinding.dataList.setAdapter(vehicleAdapter);
        mainBinding.dataList.addOnScrollListener(scrollListener);

        mainBinding.swipeRefreshLayout.setOnRefreshListener(this);

        if (savedInstanceState == null) {
            if (hasActiveInternetConnection()) {
                mainBinding.progressBar.setVisibility(View.VISIBLE);
                mainPresenter.getVehicles(1, PER_PAGE, sortKey);
            } else {
                showErrorToast("Please check your connection.");
            }
        } else {
            scrollListener.setCurrentPage(savedInstanceState.getInt(CURRENT_PAGE));
            vehicleAdapter.setData(savedInstanceState.<Vehicle>getParcelableArrayList(ITEMS));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ITEMS, vehicleAdapter.getData());
        outState.putInt(CURRENT_PAGE, scrollListener.getCurrentPage());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sort:
                displaySortKeyDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displaySortKeyDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sort by")
                .setSingleChoiceItems(R.array.sort_keys, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                sortKey = "oldest";
                                break;
                            case 1:
                                sortKey = "newest";
                                break;
                            case 2:
                                sortKey = "price-low";
                                break;
                            case 3:
                                sortKey = "price-high";
                                break;
                            case 4:
                                sortKey = "mileage-low";
                                break;
                            case 5:
                                sortKey = "mileage-high";
                                break;
                        }
                        if (!sortKey.isEmpty())
                            mainPresenter.getVehicles(1, PER_PAGE, sortKey);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        mainBinding.dataList.smoothScrollToPosition(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.destroy();
    }

    @Override
    public void showLoading() {
        mainBinding.swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void dismissLoading() {
        mainBinding.swipeRefreshLayout.setRefreshing(false);
        mainBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void loadVehicles(ArrayList<Vehicle> data, long currentPage, long totalItemsPerPage,
                             long totalPages) {
        this.totalPages = totalPages;
        if (currentPage == 1)
            vehicleAdapter.setData(data);
        else
            vehicleAdapter.append(data);
        mainBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorToast(String message) {
        showToast(message);
        mainBinding.progressBar.setVisibility(View.GONE);
        scrollListener.resetState();
    }

    @Override
    public void onRefresh() {
        if (hasActiveInternetConnection()) {
            sortKey = "";
            mainPresenter.getVehicles(1, PER_PAGE, sortKey);
            mainBinding.progressBar.setVisibility(View.GONE);
        }
    }
}
