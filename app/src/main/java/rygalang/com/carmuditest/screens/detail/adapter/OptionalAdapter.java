package rygalang.com.carmuditest.screens.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import rygalang.com.carmuditest.databinding.OptionalItemLayoutBinding;
import rygalang.com.carmuditest.model.OptionalItem;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class OptionalAdapter extends RecyclerView.Adapter<OptionalAdapter.OptionalViewHolder> {
    private List<OptionalItem> data = Collections.EMPTY_LIST;

    public OptionalAdapter(List<OptionalItem> data) {
        this.data = data;
    }

    public void setData(List<OptionalItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public OptionalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final OptionalItemLayoutBinding binding = OptionalItemLayoutBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new OptionalViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(OptionalViewHolder holder, int position) {
        final OptionalItem item = data.get(position);
        holder.itemLayoutBinding.labelName.setText(item.getName());
        final StringBuilder stringBuilder = new StringBuilder();
        for (String s : item.getOptions()) {
            stringBuilder
                    .append(s).append("\n");
        }
        holder.itemLayoutBinding.labelValue.setText(stringBuilder.toString());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class OptionalViewHolder extends RecyclerView.ViewHolder {
        private OptionalItemLayoutBinding itemLayoutBinding;

        public OptionalViewHolder(OptionalItemLayoutBinding itemLayoutBinding) {
            super(itemLayoutBinding.getRoot());
            this.itemLayoutBinding = itemLayoutBinding;
        }
    }
}
