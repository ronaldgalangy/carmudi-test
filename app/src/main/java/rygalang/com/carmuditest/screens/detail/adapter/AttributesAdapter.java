package rygalang.com.carmuditest.screens.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import rygalang.com.carmuditest.databinding.AttributeItemLayoutBinding;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.AttributesViewHolder> {
    private JsonArray jsonElements = new JsonArray();

    public AttributesAdapter(JsonArray jsonElements) {
        this.jsonElements = jsonElements;
    }

    public void setJsonElements(JsonArray jsonElements) {
        this.jsonElements = jsonElements;
        notifyDataSetChanged();
    }

    @Override
    public AttributesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final AttributeItemLayoutBinding binding = AttributeItemLayoutBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new AttributesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(AttributesViewHolder holder, int position) {
        final JsonElement jsonElement = jsonElements.get(position);
        holder.itemLayoutBinding.labelName.setText(jsonElement.getAsJsonObject().get("label_en").getAsString());
        holder.itemLayoutBinding.labelValue.setText(jsonElement.getAsJsonObject().get("value").getAsString());
    }

    @Override
    public int getItemCount() {
        return jsonElements.size();
    }

    static class AttributesViewHolder extends RecyclerView.ViewHolder {
        private AttributeItemLayoutBinding itemLayoutBinding;

        public AttributesViewHolder(AttributeItemLayoutBinding itemLayoutBinding) {
            super(itemLayoutBinding.getRoot());
            this.itemLayoutBinding = itemLayoutBinding;
        }
    }
}
