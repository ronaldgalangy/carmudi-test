package rygalang.com.carmuditest.screens.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.google.gson.JsonArray;

import java.util.List;

import javax.inject.Inject;

import rygalang.com.carmuditest.R;
import rygalang.com.carmuditest.base.BaseActivity;
import rygalang.com.carmuditest.databinding.ActivityDetailBinding;
import rygalang.com.carmuditest.model.OptionalItem;
import rygalang.com.carmuditest.model.Vehicle;
import rygalang.com.carmuditest.model.VehicleImage;
import rygalang.com.carmuditest.screens.detail.adapter.AttributesAdapter;
import rygalang.com.carmuditest.screens.detail.adapter.OptionalAdapter;
import rygalang.com.carmuditest.screens.main.VehicleAdapter;

/**
 * Created by Computer3 on 10/9/2017.
 */

public class DetailActivity extends BaseActivity implements DetailContract.DetailView {
    private ActivityDetailBinding detailBinding;
    private Vehicle vehicle;

    @Inject
    DetailPresenter detailPresenter;
    @Inject
    AttributesAdapter attributesAdapter;
    @Inject
    OptionalAdapter optionalAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null) {
            vehicle = savedInstanceState.getParcelable(VehicleAdapter.SELECTED_VEHICLE);
        } else {
            if (getIntent().hasExtra(VehicleAdapter.SELECTED_VEHICLE)) {
                vehicle = getIntent().getParcelableExtra(VehicleAdapter.SELECTED_VEHICLE);
            }
        }

        DaggerDetailComponent.builder()
                .detailModule(new DetailModule(vehicle, this))
                .build().inject(this);
        detailPresenter.getVehicleAttributes();

        detailBinding.detailsList.setNestedScrollingEnabled(false);
        detailBinding.optionalList.setNestedScrollingEnabled(false);
        detailBinding.detailsList.setAdapter(attributesAdapter);
        detailBinding.optionalList.setAdapter(optionalAdapter);

        setTitle(vehicle.getData().getName());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(VehicleAdapter.SELECTED_VEHICLE, vehicle);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detailPresenter.destroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void displayAttributes(JsonArray details) {
        attributesAdapter.setJsonElements(details);
    }

    @Override
    public void displayOptionals(List<OptionalItem> optionals) {
        optionalAdapter.setData(optionals);
    }

    @Override
    public void displayVehicleImages(List<VehicleImage> images) {
        final VehicleImageAdapter vehicleImageAdapter = new VehicleImageAdapter(getSupportFragmentManager(),
                images);
        detailBinding.vehicleImagePager.setAdapter(vehicleImageAdapter);
    }
}
