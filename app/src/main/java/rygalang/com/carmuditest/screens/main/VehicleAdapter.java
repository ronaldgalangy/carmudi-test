package rygalang.com.carmuditest.screens.main;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;

import rygalang.com.carmuditest.GlideApp;
import rygalang.com.carmuditest.databinding.VehicleItemLayoutBinding;
import rygalang.com.carmuditest.model.Vehicle;
import rygalang.com.carmuditest.model.VehicleData;
import rygalang.com.carmuditest.screens.detail.DetailActivity;

/**
 * Created by Computer3 on 10/9/2017.
 */

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder> {
    public static final String SELECTED_VEHICLE = "selected_vehicle";
    private final int ITEM_VIEW = 0;
    private final int LOADING_VIEW = 1;
    private ArrayList<Vehicle> data = new ArrayList<>();

    public VehicleAdapter(ArrayList<Vehicle> data) {
        this.data = data;
    }

    public void setData(ArrayList<Vehicle> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public ArrayList<Vehicle> getData() {
        return data;
    }

    public void append(ArrayList<Vehicle> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public VehicleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final VehicleItemLayoutBinding itemBinding = VehicleItemLayoutBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new VehicleViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(final VehicleViewHolder vehicleViewHolder, int position) {
        final Vehicle vehicle = data.get(position);
        final VehicleData vehicleData = vehicle.getData();
        vehicleViewHolder.itemLayoutBinding.productName.setText(vehicleData.getName());
        vehicleViewHolder.itemLayoutBinding.productPrice.setText("Price : " + formatPrice(vehicleData.getPrice()));
        vehicleViewHolder.itemLayoutBinding.productBrand.setText("Brand : " + vehicleData.getBrand());
        vehicleViewHolder.itemLayoutBinding.progressBar.setVisibility(View.VISIBLE);
        GlideApp.with(vehicleViewHolder.itemLayoutBinding.getRoot())
                .load(vehicle.getImages().get(0).getUrl())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        vehicleViewHolder.itemLayoutBinding.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        vehicleViewHolder.itemLayoutBinding.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(vehicleViewHolder.itemLayoutBinding.productImage);
        vehicleViewHolder.itemLayoutBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(view.getContext(), DetailActivity.class);
                intent.putExtra(SELECTED_VEHICLE, vehicle);
                view.getContext().startActivity(intent);
            }
        });
    }

    private String formatPrice(String price) {
        final DecimalFormat decimalFormat = new DecimalFormat("#,###,##0.00");
        return decimalFormat.format(Double.valueOf(price));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) == null ? LOADING_VIEW : ITEM_VIEW;
    }

    static class VehicleViewHolder extends RecyclerView.ViewHolder {
        private VehicleItemLayoutBinding itemLayoutBinding;

        public VehicleViewHolder(VehicleItemLayoutBinding itemLayoutBinding) {
            super(itemLayoutBinding.getRoot());
            this.itemLayoutBinding = itemLayoutBinding;
        }
    }
}
