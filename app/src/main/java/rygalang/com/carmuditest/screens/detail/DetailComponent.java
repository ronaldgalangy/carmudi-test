package rygalang.com.carmuditest.screens.detail;

import dagger.Component;

/**
 * Created by Computer3 on 10/10/2017.
 */
@Component(modules = {DetailModule.class})
public interface DetailComponent {
    void inject(DetailActivity detailActivity);
}
