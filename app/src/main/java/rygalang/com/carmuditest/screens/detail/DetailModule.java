package rygalang.com.carmuditest.screens.detail;

import com.google.gson.JsonArray;

import java.util.Collections;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import rygalang.com.carmuditest.model.Vehicle;
import rygalang.com.carmuditest.screens.detail.adapter.AttributesAdapter;
import rygalang.com.carmuditest.screens.detail.adapter.OptionalAdapter;

/**
 * Created by Computer3 on 10/10/2017.
 */
@Module
public class DetailModule {
    private Vehicle vehicle;
    private DetailContract.DetailView detailView;

    public DetailModule(Vehicle vehicle, DetailContract.DetailView detailView) {
        this.vehicle = vehicle;
        this.detailView = detailView;
    }

    @Provides
    public DetailContract.DetailView provideDetailView() {
        return this.detailView;
    }

    @Provides
    public Vehicle provideVehicle() {
        return this.vehicle;
    }

    @Provides
    public AttributesAdapter provideAttributesAdapter() {
        return new AttributesAdapter(new JsonArray());
    }

    @Provides
    public OptionalAdapter provideOptionalAdapter() {
        return new OptionalAdapter(Collections.EMPTY_LIST);
    }

    @Provides
    public CompositeDisposable compositeDisposable() {
        return new CompositeDisposable();
    }
}
