package rygalang.com.carmuditest.screens.detail;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import rygalang.com.carmuditest.GlideApp;
import rygalang.com.carmuditest.base.BaseFragment;
import rygalang.com.carmuditest.databinding.FragmentVehicleImageBinding;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class VehicleImageFragment extends BaseFragment<DetailActivity> {
    private FragmentVehicleImageBinding vehicleImageBinding;
    private String imageUrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vehicleImageBinding = FragmentVehicleImageBinding.inflate(inflater, container, false);

        if (savedInstanceState != null) {
            imageUrl = savedInstanceState.getString(VehicleImageAdapter.IMAGE_URL);
        } else {
            imageUrl = getArguments().getString(VehicleImageAdapter.IMAGE_URL);
        }
        GlideApp.with(this)
                .load(imageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        vehicleImageBinding.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        vehicleImageBinding.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(vehicleImageBinding.productImage);
        return vehicleImageBinding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(VehicleImageAdapter.IMAGE_URL, imageUrl);
        super.onSaveInstanceState(outState);
    }
}
