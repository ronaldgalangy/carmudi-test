package rygalang.com.carmuditest.screens.main;

import java.util.ArrayList;

import rygalang.com.carmuditest.base.BaseView;
import rygalang.com.carmuditest.model.Vehicle;

/**
 * Created by Computer3 on 10/9/2017.
 */

public interface MainContract {
    interface MainView extends BaseView {
        void loadVehicles(ArrayList<Vehicle> data, long currentPage, long totalItemsPerPage, long totalPages);
        void showErrorToast(String message);
    }

    interface MainAction {
        void getVehicles(int page, int maxItems, String sortKey);
    }
}
