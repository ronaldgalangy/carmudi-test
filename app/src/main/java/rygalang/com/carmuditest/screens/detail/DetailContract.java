package rygalang.com.carmuditest.screens.detail;

import com.google.gson.JsonArray;

import java.util.List;

import rygalang.com.carmuditest.base.BaseView;
import rygalang.com.carmuditest.model.OptionalItem;
import rygalang.com.carmuditest.model.VehicleImage;

/**
 * Created by Computer3 on 10/10/2017.
 */

public interface DetailContract {
    interface DetailView extends BaseView {

        void displayAttributes(JsonArray details);
        void displayOptionals(List<OptionalItem> optionals);
        void displayVehicleImages(List<VehicleImage> images);
    }

    interface DetailAction {
        void getVehicleAttributes();
    }
}
