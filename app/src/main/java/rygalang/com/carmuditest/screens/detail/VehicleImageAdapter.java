package rygalang.com.carmuditest.screens.detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.Collections;
import java.util.List;

import rygalang.com.carmuditest.model.VehicleImage;

/**
 * Created by Computer3 on 10/10/2017.
 */

public class VehicleImageAdapter extends FragmentStatePagerAdapter {
    public static final String IMAGE_URL = "image_url";
    private List<VehicleImage> data = Collections.EMPTY_LIST;

    public VehicleImageAdapter(FragmentManager fm, List<VehicleImage> data) {
        super(fm);
        this.data = data;
    }

    public void setData(List<VehicleImage> data) {
        this.data = data;
    }

    @Override
    public Fragment getItem(int position) {
        final Fragment fragment = new VehicleImageFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(IMAGE_URL, data.get(position).getUrl());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
