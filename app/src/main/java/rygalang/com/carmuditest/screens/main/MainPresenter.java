package rygalang.com.carmuditest.screens.main;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import rygalang.com.carmuditest.ApiService;
import rygalang.com.carmuditest.BuildConfig;
import rygalang.com.carmuditest.base.BasePresenter;
import rygalang.com.carmuditest.model.VehicleResponse;

/**
 * Created by Computer3 on 10/9/2017.
 */

public class MainPresenter extends BasePresenter implements MainContract.MainAction {
    private final CompositeDisposable compositeDisposable;
    private final ApiService apiService;
    private final MainContract.MainView mainView;

    @Inject
    public MainPresenter(MainContract.MainView mainView, ApiService apiService, CompositeDisposable compositeDisposable) {
        this.apiService = apiService;
        this.mainView = mainView;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void getVehicles(final int page, final int maxItems, String sortKey) {
        final Disposable disposable = apiService.getVehicles(page, maxItems, sortKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Consumer<VehicleResponse>() {
                    @Override
                    public void accept(VehicleResponse vehicleResponse) throws Exception {
                        if (vehicleResponse.isSuccess()) {
                            final long productCount = vehicleResponse.getMetaData().getProductCount();
                            final long totalPages = productCount / maxItems;
                            mainView.loadVehicles(vehicleResponse.getMetaData().getResults(), page, maxItems, totalPages);
                        } else {
                            mainView.showErrorToast("Something went wrong. Please try again.");
                        }
                        mainView.dismissLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mainView.dismissLoading();
                        mainView.showErrorToast(throwable.getMessage());
                        if (BuildConfig.DEBUG)
                            throwable.printStackTrace();
                    }
                });
        compositeDisposable.add(disposable);
    }

    @Override
    protected void destroy() {
        compositeDisposable.clear();
    }
}
