package rygalang.com.carmuditest.screens.main;

import dagger.Component;
import rygalang.com.carmuditest.component.NetComponent;
import rygalang.com.carmuditest.util.CustomScope;

/**
 * Created by Computer3 on 10/9/2017.
 */
@CustomScope
@Component(modules = {MainModule.class}, dependencies = {NetComponent.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
