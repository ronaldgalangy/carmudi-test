package rygalang.com.carmuditest;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rygalang.com.carmuditest.model.VehicleResponse;

/**
 * Created by Computer3 on 10/6/2017.
 */

public interface ApiService {
    @GET("cars/page:{pageNumber}/maxitems:{maxItems}/sort:{sortKey}")
    Observable<VehicleResponse> getVehicles(@Path("pageNumber") int pageNumber,
                                            @Path("maxItems") int maxItems,
                                            @Path("sortKey") String sortKey);
}
