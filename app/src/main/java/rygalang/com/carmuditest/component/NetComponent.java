package rygalang.com.carmuditest.component;

import javax.inject.Singleton;

import dagger.Component;
import rygalang.com.carmuditest.ApiService;
import rygalang.com.carmuditest.modules.AppModule;
import rygalang.com.carmuditest.modules.NetModule;
import rygalang.com.carmuditest.util.NetworkUtils;
import rygalang.com.carmuditest.util.PrefsHelper;

/**
 * Created by Computer3 on 10/6/2017.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    ApiService getApiService();
    PrefsHelper getPrefsHelper();
    NetworkUtils getNetworkUtils();
}
