package rygalang.com.carmuditest.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rygalang.com.carmuditest.App;

/**
 * Created by Computer3 on 10/6/2017.
 * AppModule that will provide context to other modules.
 */
@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApp() {
        return app;
    }
}
