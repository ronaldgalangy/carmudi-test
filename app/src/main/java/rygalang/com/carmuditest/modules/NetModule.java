package rygalang.com.carmuditest.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rygalang.com.carmuditest.ApiService;
import rygalang.com.carmuditest.App;
import rygalang.com.carmuditest.BuildConfig;
import rygalang.com.carmuditest.util.NetworkUtils;
import rygalang.com.carmuditest.util.PrefsHelper;

/**
 * Created by Computer3 on 10/6/2017.
 * NetModule that will provide Retrofit and OkHttp
 */
@Module
public class NetModule {
    private String baseUrl;

    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    Cache provideHttpCache(App app) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(app.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.setLenient().create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {
        return new OkHttpClient.Builder().build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(this.baseUrl)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }


    @Provides
    @Singleton
    PrefsHelper providePrefsHelper(App app) {
        return new PrefsHelper(app.getSharedPreferences(BuildConfig.APPLICATION_ID, app.MODE_PRIVATE));
    }

    @Provides
    @Singleton
    NetworkUtils provideNetworkUtils(App app) {
        return new NetworkUtils(app);
    }
}
